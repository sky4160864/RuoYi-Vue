package com.ruoyi.demo.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 站位对象 demo_station
 *
 * @author ruoyi-c
 * @date 2020-10-19
 */
public class DemoStation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键自增长 */
    private Long id;

    /** 站位编号 */
    @Excel(name = "站位编号")
    private String stationId;

    /** 站位名称 */
    @Excel(name = "站位名称")
    private String stationName;

    /** 站位IP */
    @Excel(name = "站位IP")
    private String stationIp;

    /** 站位地址 */
    @Excel(name = "站位地址")
    private String stationAddr;

    /** 行政区划 */
    @Excel(name = "行政区划")
    private String areaName;

    /** 行政区划编码 */
    @Excel(name = "行政区划编码")
    private String areaCode;

    /** 行业代码 */
    @Excel(name = "行业代码")
    private String tradeCode;

    /** 法人姓名 */
    @Excel(name = "法人姓名")
    private String deputy;

    /** 法人电话 */
    @Excel(name = "法人电话")
    private String deputyPhone;

    /** 工商注册码 */
    @Excel(name = "工商注册码")
    private String regCode;

    /** 法人代码 */
    @Excel(name = "法人代码")
    private String epCode;

    /** 联系人 */
    @Excel(name = "联系人")
    private String linkMan;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String linkPhone;

    /** 企业状态: 0正常,1停产,2关闭 */
    @Excel(name = "企业状态")
    private Integer stationStatus;

    /** 经度 */
    @Excel(name = "经度")
    private BigDecimal longitude;

    /** 纬度 */
    @Excel(name = "纬度")
    private BigDecimal latitude;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setStationId(String stationId)
    {
        this.stationId = stationId;
    }

    public String getStationId()
    {
        return stationId;
    }
    public void setStationName(String stationName)
    {
        this.stationName = stationName;
    }

    public String getStationName()
    {
        return stationName;
    }
    public void setStationIp(String stationIp)
    {
        this.stationIp = stationIp;
    }

    public String getStationIp()
    {
        return stationIp;
    }
    public void setStationAddr(String stationAddr)
    {
        this.stationAddr = stationAddr;
    }

    public String getStationAddr()
    {
        return stationAddr;
    }
    public void setAreaName(String areaName)
    {
        this.areaName = areaName;
    }

    public String getAreaName()
    {
        return areaName;
    }
    public void setAreaCode(String areaCode)
    {
        this.areaCode = areaCode;
    }

    public String getAreaCode()
    {
        return areaCode;
    }
    public void setTradeCode(String tradeCode)
    {
        this.tradeCode = tradeCode;
    }

    public String getTradeCode()
    {
        return tradeCode;
    }
    public void setDeputy(String deputy)
    {
        this.deputy = deputy;
    }

    public String getDeputy()
    {
        return deputy;
    }
    public void setDeputyPhone(String deputyPhone)
    {
        this.deputyPhone = deputyPhone;
    }

    public String getDeputyPhone()
    {
        return deputyPhone;
    }
    public void setRegCode(String regCode)
    {
        this.regCode = regCode;
    }

    public String getRegCode()
    {
        return regCode;
    }
    public void setEpCode(String epCode)
    {
        this.epCode = epCode;
    }

    public String getEpCode()
    {
        return epCode;
    }
    public void setLinkMan(String linkMan)
    {
        this.linkMan = linkMan;
    }

    public String getLinkMan()
    {
        return linkMan;
    }
    public void setLinkPhone(String linkPhone)
    {
        this.linkPhone = linkPhone;
    }

    public String getLinkPhone()
    {
        return linkPhone;
    }
    public void setStationStatus(Integer stationStatus)
    {
        this.stationStatus = stationStatus;
    }

    public Integer getStationStatus()
    {
        return stationStatus;
    }
    public void setLongitude(BigDecimal longitude)
    {
        this.longitude = longitude;
    }

    public BigDecimal getLongitude()
    {
        return longitude;
    }
    public void setLatitude(BigDecimal latitude)
    {
        this.latitude = latitude;
    }

    public BigDecimal getLatitude()
    {
        return latitude;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("stationId", getStationId())
            .append("stationName", getStationName())
            .append("stationIp", getStationIp())
            .append("stationAddr", getStationAddr())
            .append("areaName", getAreaName())
            .append("areaCode", getAreaCode())
            .append("tradeCode", getTradeCode())
            .append("deputy", getDeputy())
            .append("deputyPhone", getDeputyPhone())
            .append("regCode", getRegCode())
            .append("epCode", getEpCode())
            .append("linkMan", getLinkMan())
            .append("linkPhone", getLinkPhone())
            .append("stationStatus", getStationStatus())
            .append("longitude", getLongitude())
            .append("latitude", getLatitude())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
