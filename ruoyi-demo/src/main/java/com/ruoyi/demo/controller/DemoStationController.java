package com.ruoyi.demo.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.demo.domain.DemoStation;
import com.ruoyi.demo.service.IDemoStationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 站位Controller
 * 
 * @author ruoyi-c
 * @date 2020-10-19
 */
@RestController
@RequestMapping("/demo/station")
public class DemoStationController extends BaseController
{
    @Autowired
    private IDemoStationService demoStationService;

    /**
     * 查询站位列表
     */
    @PreAuthorize("@ss.hasPermi('demo:station:list')")
    @GetMapping("/list")
    public TableDataInfo list(DemoStation demoStation)
    {
        startPage();
        List<DemoStation> list = demoStationService.selectDemoStationList(demoStation);
        return getDataTable(list);
    }

    /**
     * 导出站位列表
     */
    @PreAuthorize("@ss.hasPermi('demo:station:export')")
    @Log(title = "站位", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DemoStation demoStation)
    {
        List<DemoStation> list = demoStationService.selectDemoStationList(demoStation);
        ExcelUtil<DemoStation> util = new ExcelUtil<DemoStation>(DemoStation.class);
        return util.exportExcel(list, "station");
    }

    /**
     * 获取站位详细信息
     */
    @PreAuthorize("@ss.hasPermi('demo:station:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(demoStationService.selectDemoStationById(id));
    }

    /**
     * 新增站位
     */
    @PreAuthorize("@ss.hasPermi('demo:station:add')")
    @Log(title = "站位", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DemoStation demoStation)
    {
        return toAjax(demoStationService.insertDemoStation(demoStation));
    }

    /**
     * 修改站位
     */
    @PreAuthorize("@ss.hasPermi('demo:station:edit')")
    @Log(title = "站位", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DemoStation demoStation)
    {
        return toAjax(demoStationService.updateDemoStation(demoStation));
    }

    /**
     * 删除站位
     */
    @PreAuthorize("@ss.hasPermi('demo:station:remove')")
    @Log(title = "站位", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(demoStationService.deleteDemoStationByIds(ids));
    }
}
