package com.ruoyi.demo.service.impl;

import java.util.List;

import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.demo.mapper.DemoStationMapper;
import com.ruoyi.demo.domain.DemoStation;
import com.ruoyi.demo.service.IDemoStationService;

/**
 * 站位Service业务层处理
 *
 * @author ruoyi-c
 * @date 2020-10-19
 */
@Service
public class DemoStationServiceImpl implements IDemoStationService
{
    @Autowired
    private DemoStationMapper demoStationMapper;

    /**
     * 查询站位
     *
     * @param id 站位ID
     * @return 站位
     */
    @Override
    public DemoStation selectDemoStationById(Long id)
    {
        return demoStationMapper.selectDemoStationById(id);
    }

    /**
     * 查询站位列表
     *
     * @param demoStation 站位
     * @return 站位
     */
    @Override
    @DataScope(deptAlias = "d", userAlias = "u")
    public List<DemoStation> selectDemoStationList(DemoStation demoStation)
    {
        return demoStationMapper.selectDemoStationList(demoStation);
    }

    /**
     * 新增站位
     *
     * @param demoStation 站位
     * @return 结果
     */
    @Override
    public int insertDemoStation(DemoStation demoStation)
    {
        demoStation.setCreateTime(DateUtils.getNowDate());
        return demoStationMapper.insertDemoStation(demoStation);
    }

    /**
     * 修改站位
     *
     * @param demoStation 站位
     * @return 结果
     */
    @Override
    public int updateDemoStation(DemoStation demoStation)
    {
        demoStation.setUpdateTime(DateUtils.getNowDate());
        return demoStationMapper.updateDemoStation(demoStation);
    }

    /**
     * 批量删除站位
     *
     * @param ids 需要删除的站位ID
     * @return 结果
     */
    @Override
    public int deleteDemoStationByIds(Long[] ids)
    {
        return demoStationMapper.deleteDemoStationByIds(ids);
    }

    /**
     * 删除站位信息
     *
     * @param id 站位ID
     * @return 结果
     */
    @Override
    public int deleteDemoStationById(Long id)
    {
        return demoStationMapper.deleteDemoStationById(id);
    }
}
