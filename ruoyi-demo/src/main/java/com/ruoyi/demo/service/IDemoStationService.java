package com.ruoyi.demo.service;

import java.util.List;
import com.ruoyi.demo.domain.DemoStation;

/**
 * 站位Service接口
 * 
 * @author ruoyi-c
 * @date 2020-10-19
 */
public interface IDemoStationService 
{
    /**
     * 查询站位
     * 
     * @param id 站位ID
     * @return 站位
     */
    public DemoStation selectDemoStationById(Long id);

    /**
     * 查询站位列表
     * 
     * @param demoStation 站位
     * @return 站位集合
     */
    public List<DemoStation> selectDemoStationList(DemoStation demoStation);

    /**
     * 新增站位
     * 
     * @param demoStation 站位
     * @return 结果
     */
    public int insertDemoStation(DemoStation demoStation);

    /**
     * 修改站位
     * 
     * @param demoStation 站位
     * @return 结果
     */
    public int updateDemoStation(DemoStation demoStation);

    /**
     * 批量删除站位
     * 
     * @param ids 需要删除的站位ID
     * @return 结果
     */
    public int deleteDemoStationByIds(Long[] ids);

    /**
     * 删除站位信息
     * 
     * @param id 站位ID
     * @return 结果
     */
    public int deleteDemoStationById(Long id);
}
