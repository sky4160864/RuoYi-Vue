package com.ruoyi.web.controller.common;


import com.google.code.kaptcha.Producer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.awt.image.BufferedImage;

/**
 * @author C.H
 * @date 2020/11/4 11:27
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CaptchaControllerTest {
    @Resource(name = "captchaProducerMath")
    private Producer captchaProducerMath;

    @Test
    public void test(){
        String capText = captchaProducerMath.createText();
        String capStr = capText.substring(0, capText.lastIndexOf("@"));
        String code = capText.substring(capText.lastIndexOf("@") + 1);
        BufferedImage image = captchaProducerMath.createImage(capStr);
        System.out.println();
    }
}
