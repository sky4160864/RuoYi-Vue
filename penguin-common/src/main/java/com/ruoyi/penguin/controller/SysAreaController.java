package com.ruoyi.penguin.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.penguin.domain.SysArea;
import com.ruoyi.penguin.service.ISysAreaService;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 * 系统区域Controller
 * 
 * @author ruoyi-c
 * @date 2020-10-20
 */
@RestController
@RequestMapping("/penguin/area")
public class SysAreaController extends BaseController
{
    @Autowired
    private ISysAreaService sysAreaService;

    /**
     * 查询系统区域列表
     */
    @PreAuthorize("@ss.hasPermi('penguin:area:list')")
    @GetMapping("/list")
    public AjaxResult list(SysArea sysArea)
    {
        List<SysArea> list = sysAreaService.selectSysAreaList(sysArea);
        return AjaxResult.success(list);
    }

    /**
     * 导出系统区域列表
     */
    @PreAuthorize("@ss.hasPermi('penguin:area:export')")
    @Log(title = "系统区域", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysArea sysArea)
    {
        List<SysArea> list = sysAreaService.selectSysAreaList(sysArea);
        ExcelUtil<SysArea> util = new ExcelUtil<SysArea>(SysArea.class);
        return util.exportExcel(list, "area");
    }

    /**
     * 获取系统区域详细信息
     */
    @PreAuthorize("@ss.hasPermi('penguin:area:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(sysAreaService.selectSysAreaById(id));
    }

    /**
     * 新增系统区域
     */
    @PreAuthorize("@ss.hasPermi('penguin:area:add')")
    @Log(title = "系统区域", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysArea sysArea)
    {
        return toAjax(sysAreaService.insertSysArea(sysArea));
    }

    /**
     * 修改系统区域
     */
    @PreAuthorize("@ss.hasPermi('penguin:area:edit')")
    @Log(title = "系统区域", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysArea sysArea)
    {
        return toAjax(sysAreaService.updateSysArea(sysArea));
    }

    /**
     * 删除系统区域
     */
    @PreAuthorize("@ss.hasPermi('penguin:area:remove')")
    @Log(title = "系统区域", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sysAreaService.deleteSysAreaByIds(ids));
    }
}
