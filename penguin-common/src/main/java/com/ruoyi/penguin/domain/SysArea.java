package com.ruoyi.penguin.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.TreeEntity;

/**
 * 系统区域对象 sys_area
 * 
 * @author ruoyi-c
 * @date 2020-10-20
 */
public class SysArea extends TreeEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键自增长 */
    private Long id;

    /** 区域编码 */
    @Excel(name = "区域编码")
    private String areaCode;

    /** 区域名称 */
    @Excel(name = "区域名称")
    private String areaName;

    /** 父区域编码 */
    @Excel(name = "父区域编码")
    private String parentAreaCode;

    /** 父区域名称 */
    @Excel(name = "父区域名称")
    private String parentAreaName;

    /** 级别:1区县,2市,3省 */
    @Excel(name = "级别:1区县,2市,3省")
    private Long level;

    /** 排序号 */
    @Excel(name = "排序号")
    private String orderId;

    /** 经度 */
    @Excel(name = "经度")
    private BigDecimal longitude;

    /** 经度 */
    @Excel(name = "经度")
    private BigDecimal latitude;

    /** 是否启用,1 启用，0 禁用 */
    @Excel(name = "是否启用,1 启用，0 禁用")
    private Integer enable;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAreaCode(String areaCode) 
    {
        this.areaCode = areaCode;
    }

    public String getAreaCode() 
    {
        return areaCode;
    }
    public void setAreaName(String areaName) 
    {
        this.areaName = areaName;
    }

    public String getAreaName() 
    {
        return areaName;
    }
    public void setParentAreaCode(String parentAreaCode) 
    {
        this.parentAreaCode = parentAreaCode;
    }

    public String getParentAreaCode() 
    {
        return parentAreaCode;
    }
    public void setParentAreaName(String parentAreaName) 
    {
        this.parentAreaName = parentAreaName;
    }

    public String getParentAreaName() 
    {
        return parentAreaName;
    }
    public void setLevel(Long level) 
    {
        this.level = level;
    }

    public Long getLevel() 
    {
        return level;
    }
    public void setOrderId(String orderId) 
    {
        this.orderId = orderId;
    }

    public String getOrderId() 
    {
        return orderId;
    }
    public void setLongitude(BigDecimal longitude) 
    {
        this.longitude = longitude;
    }

    public BigDecimal getLongitude() 
    {
        return longitude;
    }
    public void setLatitude(BigDecimal latitude) 
    {
        this.latitude = latitude;
    }

    public BigDecimal getLatitude() 
    {
        return latitude;
    }
    public void setEnable(Integer enable) 
    {
        this.enable = enable;
    }

    public Integer getEnable() 
    {
        return enable;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("areaCode", getAreaCode())
            .append("areaName", getAreaName())
            .append("parentAreaCode", getParentAreaCode())
            .append("parentAreaName", getParentAreaName())
            .append("level", getLevel())
            .append("orderId", getOrderId())
            .append("longitude", getLongitude())
            .append("latitude", getLatitude())
            .append("enable", getEnable())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
