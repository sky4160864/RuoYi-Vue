package com.ruoyi.penguin.service;

import java.util.List;
import com.ruoyi.penguin.domain.SysArea;

/**
 * 系统区域Service接口
 * 
 * @author ruoyi-c
 * @date 2020-10-20
 */
public interface ISysAreaService 
{
    /**
     * 查询系统区域
     * 
     * @param id 系统区域ID
     * @return 系统区域
     */
    public SysArea selectSysAreaById(Long id);

    /**
     * 查询系统区域列表
     * 
     * @param sysArea 系统区域
     * @return 系统区域集合
     */
    public List<SysArea> selectSysAreaList(SysArea sysArea);

    /**
     * 新增系统区域
     * 
     * @param sysArea 系统区域
     * @return 结果
     */
    public int insertSysArea(SysArea sysArea);

    /**
     * 修改系统区域
     * 
     * @param sysArea 系统区域
     * @return 结果
     */
    public int updateSysArea(SysArea sysArea);

    /**
     * 批量删除系统区域
     * 
     * @param ids 需要删除的系统区域ID
     * @return 结果
     */
    public int deleteSysAreaByIds(Long[] ids);

    /**
     * 删除系统区域信息
     * 
     * @param id 系统区域ID
     * @return 结果
     */
    public int deleteSysAreaById(Long id);
}
