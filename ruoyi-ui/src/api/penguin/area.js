import request from '@/utils/request'

// 查询系统区域列表
export function listArea(query) {
  return request({
    url: '/penguin/area/list',
    method: 'get',
    params: query
  })
}

// 查询系统区域详细
export function getArea(id) {
  return request({
    url: '/penguin/area/' + id,
    method: 'get'
  })
}

// 新增系统区域
export function addArea(data) {
  return request({
    url: '/penguin/area',
    method: 'post',
    data: data
  })
}

// 修改系统区域
export function updateArea(data) {
  return request({
    url: '/penguin/area',
    method: 'put',
    data: data
  })
}

// 删除系统区域
export function delArea(id) {
  return request({
    url: '/penguin/area/' + id,
    method: 'delete'
  })
}

// 导出系统区域
export function exportArea(query) {
  return request({
    url: '/penguin/area/export',
    method: 'get',
    params: query
  })
}