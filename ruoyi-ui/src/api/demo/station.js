import request from '@/utils/request'

// 查询站位列表
export function listStation(query) {
  return request({
    url: '/demo/station/list',
    method: 'get',
    params: query
  })
}

// 查询站位详细
export function getStation(id) {
  return request({
    url: '/demo/station/' + id,
    method: 'get'
  })
}

// 新增站位
export function addStation(data) {
  return request({
    url: '/demo/station',
    method: 'post',
    data: data
  })
}

// 修改站位
export function updateStation(data) {
  return request({
    url: '/demo/station',
    method: 'put',
    data: data
  })
}

// 删除站位
export function delStation(id) {
  return request({
    url: '/demo/station/' + id,
    method: 'delete'
  })
}

// 导出站位
export function exportStation(query) {
  return request({
    url: '/demo/station/export',
    method: 'get',
    params: query
  })
}